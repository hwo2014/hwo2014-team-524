import json, socket, sys


class Track(object):

	def __init__(self, data):
		self.pieces = []
		for piece in data['pieces']:
			if 'length' in piece:
				self.pieces += [[0, [float(piece['length'])]]]							# 0 is straight
			else:
				self.pieces += [[1, [float(piece['radius']), float(piece['angle'])]]]	# 1 is bend
		self.lanes = []
		for lane in data['lanes']:
			self.lanes += [float(lane['distanceFromCenter'])]

class Car(object):

	def __init__(self, data):
		self.color = str(data['id']['color'])
		self.dimensions = [float(data['dimensions'][i]) for i in sorted(data['dimensions'].keys())] # [guide, length, width]

	def update(self, data):
		pass

class One(Car):

	def __init__(self, car):
		self.car = car

	def throttle(self, throttle):
		pass

class Server(object):

	def __init__(self, socket, name, key):
		self.socket = socket
		self.name = name
		self.key = key
		self.error = False

	def msg(self, msg_type, data):
		self.send(json.dumps({'msgType': msg_type, 'data': data}))

	def send(self, msg):
		self.socket.send(msg + '\n')

	def join(self):
		return self.msg('join', {'name': self.name, 'key': self.key})

	def throttle(self, throttle):
		self.msg('throttle', throttle)

	def ping(self):
		self.msg('ping', {})

	def run(self):
		self.join()
		self.msg_loop()

	def on_join(self, data, tick):
		print('Joined')
		self.ping()

	def on_game_start(self, data, tick):
		print('Race started')
		self.ping()

	def on_car_positions(self, data, tick):
		self.throttle(0.5)
		print tick

	def on_crash(self, data, tick):
		print('Someone crashed')
		self.ping()

	def on_game_end(self, data, tick):
		print('Race ended')
		self.ping()

	def on_error(self, data, tick):
		print('Error: {0}'.format(data))
		self.ping()

	def on_your_car(self, data, tick):
		self.one = [data['name'], data['color']]

	def on_game_init(self, data, tick):
		self.track = Track(data['race']['track'])
		self.cars = {car['id']['name']: Car(car) for car in data['race']['cars']}
		if self.cars[self.one[0]].color == self.one[1]:
			self.one = One(self.cars.pop(self.one[0]))
			print('Car found')
		else:
			self.error = True
			print('Couldn\'t find car!')

	def msg_loop(self):
		msg_map = {
			'join': self.on_join,
			'gameStart': self.on_game_start,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'gameEnd': self.on_game_end,
			'error': self.on_error,
			'yourCar': self.on_your_car,
			'gameInit': self.on_game_init
		}
		socket_file = s.makefile()
		line = socket_file.readline()
		while line and not self.error:
			msg = json.loads(line)
			msg_type, data = msg['msgType'], msg['data']
			if 'gameTick' in msg:
				tick = msg['gameTick']
			else:
				tick = None
			if msg_type in msg_map:
				msg_map[msg_type](data, tick)
			else:
				print('Got {0}'.format(msg_type))
				self.ping()
			line = socket_file.readline()


if __name__ == '__main__':
	if len(sys.argv) != 5:
		print('Usage: ./run host port botname botkey')
	else:
		host, port, name, key = sys.argv[1:5]
		print('Connecting')
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, int(port)))
		bot = Server(s, name, key)
		bot.run()
